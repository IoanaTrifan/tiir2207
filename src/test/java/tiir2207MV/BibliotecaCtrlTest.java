package tiir2207MV;

import org.junit.Before;
import org.junit.Test;
import tiir2207MV.control.BibliotecaCtrl;
import tiir2207MV.model.Carte;
import tiir2207MV.repository.repo.CartiRepo;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BibliotecaCtrlTest {
    private BibliotecaCtrl cartiController;

    @Before
    public void setUp() throws Exception {
        CartiRepo repository=new CartiRepo();
        cartiController=new BibliotecaCtrl(repository);
    }



//ECP VALID
    @Test
    public void test1() throws Exception {
        Carte carte = new Carte();
        int size=cartiController.getCarti().size();

        ArrayList<String> autori = new ArrayList<>();
        autori.add("AutorUnu");
        autori.add("AutorDoi");

        ArrayList<String> cuvinte = new ArrayList<>();
        cuvinte.add("da");
        cuvinte.add("nu");

        carte.setTitlu("Carte");
        carte.setReferenti(autori);
        carte.setAnAparitie("2013");
        carte.setEditura("EdituraUnu");
        carte.setCuvinteCheie(cuvinte);

        cartiController.adaugaCarte(carte);
        assertEquals(size+1, cartiController.getCarti().size());
    }


    //ECP nev
    @Test
    public void test2() throws Exception {
        Carte carte = new Carte();
        int size=cartiController.getCarti().size();

        ArrayList<String> autori = new ArrayList<>();
        autori.add("AutorTrei");

        ArrayList<String> cuvinte = new ArrayList<>();
        cuvinte.add("da");
        cuvinte.add("sigur");
        cuvinte.add("poate");

        carte.setTitlu("Titlu2");
        carte.setReferenti(autori);
        carte.setAnAparitie("2000");
        carte.setEditura("EdituraDoi");
        carte.setCuvinteCheie(cuvinte);

        try{
            cartiController.adaugaCarte(carte);
        }catch(Exception e){
            assertEquals(e.getMessage(), "Titlu invalid!");

            System.out.println(e.getMessage());
        }
        assertEquals(size, cartiController.getCarti().size());
    }



    @Test
    public void test3() throws Exception {
        Carte carte = new Carte();
        int size=cartiController.getCarti().size();

        ArrayList<String> autori = new ArrayList<>();
        autori.add("AutorPatru");
        autori.add("AutorCinci");
        autori.add("AutorSase");

        ArrayList<String> cuvinte = new ArrayList<>();
        cuvinte.add("da");

        carte.setTitlu("Carticica");
        carte.setReferenti(autori);
        carte.setAnAparitie("201p");
        carte.setEditura("EdituraTrei");
        carte.setCuvinteCheie(cuvinte);

        try{
            cartiController.adaugaCarte(carte);
        }
        catch(Exception e){
            assertEquals(e.getMessage(), "An aparitie invalid!");
            System.out.println(e.getMessage());
        }
        assertEquals(size, cartiController.getCarti().size());
    }


    //bva nev

    @Test
    public void test4() throws Exception {
        Carte carte = new Carte();
        int size=cartiController.getCarti().size();

        ArrayList<String> autori = new ArrayList<>();
        autori.add("AutorUnu");

        ArrayList<String> cuvinte = new ArrayList<>();
        cuvinte.add("da");

        carte.setTitlu("");
        carte.setReferenti(autori);
        carte.setAnAparitie("2000");
        carte.setEditura("EdituraUnu");
        carte.setCuvinteCheie(cuvinte);

        try{
            cartiController.adaugaCarte(carte);
        }catch(Exception e){
            assertEquals(e.getMessage(), "Titlu invalid!");

            System.out.println(e.getMessage());
        }
        assertEquals(size, cartiController.getCarti().size());
    }


    @Test
    public void test5() throws Exception {
        Carte carte = new Carte();
        int size=cartiController.getCarti().size();

        ArrayList<String> autori = new ArrayList<>();
        autori.add("AutorCinci");
        autori.add("AutorSase");

        ArrayList<String> cuvinte = new ArrayList<>();
        cuvinte.add("da");
        cuvinte.add("nu");

        carte.setTitlu("Titlu Carte");
        carte.setReferenti(autori);
        carte.setAnAparitie("9");
        carte.setEditura("EdituraPatru");
        carte.setCuvinteCheie(cuvinte);

        cartiController.adaugaCarte(carte);
        assertEquals(size+1, cartiController.getCarti().size());
    }


    @Test
    public void test6() throws Exception {
        Carte carte = new Carte();
        int size=cartiController.getCarti().size();

        ArrayList<String> autori = new ArrayList<>();
        autori.add("");
        autori.add("");

        ArrayList<String> cuvinte = new ArrayList<>();
        cuvinte.add("poate");

        carte.setTitlu("Carte foarte mare");
        carte.setReferenti(autori);
        carte.setAnAparitie("2009");
        carte.setEditura("EdituraTrei");
        carte.setCuvinteCheie(cuvinte);

        try {
            cartiController.adaugaCarte(carte);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        assertEquals(size, cartiController.getCarti().size());
    }
}
